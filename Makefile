.phony: default
default:
	@set -ev
	@kubectl apply -k flux-system
	@for app in nodezoo-npm-service nodezoo-github-service nodezoo-info-service; do \
		kubectl apply -f $${app}/source.yaml; \
		kubectl apply -f $${app}/service-kustomization.yaml; \
	done
