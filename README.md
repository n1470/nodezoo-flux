# nodezoo-flux

Flux configuration for Nodezoo

## About Flux

[Flux](https://fluxcd.io/) is a CD framework for Kubernetes. Flux works with [Kustomize](https://kubernetes.io/docs/tasks/manage-kubernetes-objects/kustomization/) which is already part of the Kubernetes platform.

## Architecture

[![Architecture Diagram](https://viewer.diagrams.net/?view=_blank#Uhttps%3A%2F%2Fgitlab.com%2Fn1470%2Fnodezoo-flux%2F-%2Fraw%2Fmain%2Fflux-cd-cdk8s-services.drawio)](https://viewer.diagrams.net/?view=_blank#Uhttps%3A%2F%2Fgitlab.com%2Fn1470%2Fnodezoo-flux%2F-%2Fraw%2Fmain%2Fflux-cd-cdk8s-services.drawio)

* Each application's k8s manifests are stored in a branch on a single manifest repo.
* This repo sets up the git sources and the configuration for how these sources are to be queried by Kubernetes for changes.
* When changes are detected in the manifest repo branch(es), their associated K8s configuration is changed automatically.

## Adding a new Nodezoo service

1. Copy any ``nodezoo-*-service`` folder and rename to service name such as ``nodezoo-github-service``.
2. Update the ``metadata.name`` and ``spec.ref.branch`` in service folder/``source.yml`` as per the application service name. Optionally, increase or decrease the ``spec.interval``.
3. Next, edit the ``metadata-name`` and ``spec.sourceref.name`` as per the ``source:metadata.name``.

## Setting up

``kubectl`` should be setup with appropriate K8s installation.

```shell
make
```
